FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Ben Jones <ben.dylan.jones@cern.ch>

RUN yum install -y openafs-client   \
		   kmod-openafs     \
		   krb5-workstation \
		   openafs-krb5     \
		   cern-afs-setserverprefs

ADD sysconfig/openafs /etc/sysconfig/openafs
ADD CellServDB /usr/vice/etc/
ADD CellServDB.dist /usr/vice/etc/
ADD CellServDB.local /usr/vice/etc/
ADD ThisCell /usr/vice/etc/
RUN /bin/chmod 0644 /usr/vice/etc/ThisCell
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.dist
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.local
RUN /bin/mkdir -p /afs

ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
